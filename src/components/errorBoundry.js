import React, { Component } from 'react';
import { render } from 'react-dom';

export default class ErrorBoundary extends Component {
  state = {
    error: true
  }
  componentDidCatch(error, info) {
    this.setState({ error: true });
  }
  render() {
    if (this.state.error) {
      // You can render any custom fallback UI
      return <h1>There was an Error!</h1>;
    }
    return this.props.children;
  }
}
// class DataBox extends Component {
//   state = {
//     data: []
//   }
//   componentDidMount() {
//     // Deliberately throwing an error for demo purposes
//     throw Error("I threw an error!");
//   }
//   render() {
//     return 'This App is working!'
//   }
// }
// const Error = () => <ErrorBoundary><DataBox /></ErrorBoundary>
// export default Error;