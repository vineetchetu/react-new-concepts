import React, { useReducer } from 'react';

const reducer = (state, action) => {
    switch (action.type) {
        case 'ticktock':
            return { count: state.count + 1 };
        case 'reset':
            return { count: 0 };
    }
}
let timer;
const Reducer = () => {
    const [state, dispatch] = useReducer(reducer, { count: 0 });
    return (
        <>
            <h1>{state.count}</h1>
            <button onClick={() => {
                timer = setInterval(() => dispatch({ type: 'ticktock' }), 1000);
            }}>Start</button>
            <button onClick={() => {
                clearInterval(timer);
                dispatch({ type: 'reset' });
            }}>Stop&Reset</button>
        </>);
}

export default Reducer;