import React, { Component, lazy, Suspense } from 'react';
//import "./style.css";

// This component will be imported when it is required to be rendered
const Display = lazy(() => import("./display"));

// We use Suspense to display a fallback while our Lazy component loads up.
class Lazy extends Component {
  state = {
    show: false
  }
  render() {
    return (
      <>
      {this.state.show ? 
        <Suspense fallback={<p>One moment...</p>}>
          <Display />
        </Suspense> 
        : null
      }
      <button onClick={() => this.setState({show: true})}>Show Message!</button>
      </>
    );
  }
}

export default Lazy;