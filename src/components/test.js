import React, { Component } from 'react';

const FruitsList = props => props.fruits.map((fruit, index) => <li key={index}>{fruit}</li>);
class Test extends Component {
  constructor() {
    super();
    this.state = {
      fruits: ["Apple", "Mango", "Kiwi", "Strawberry", "Banana"]
    };
  }
  render() {
    return (
      <ul>
        <FruitsList fruits={this.state.fruits} />
      </ul>
    );
  }
}

export default Test;