import React from 'react';

const Fragment = () => (
    <>
        <p>Apple</p>
        <p>Kiwi</p>
        <p>Strawberry</p>
    </>
)

export default Fragment;
