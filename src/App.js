import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Test from './components/test';
import Error from './components/errorBoundry';
import Lazy from './components/lazy';
import Fragment from './components/fragment';
import Hooks from './components/hooks';
import Reducer from './components/useReducer'
import Effect from './components/effect'
import Ref from './components/ref'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <p>
            Welcome to Kronos
            {/* Edit <code>src/App.js</code> and save to reload. */}
          </p>
          {/* <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a> */}
          <Test />
          <Error />
          <Lazy />
          <Fragment />
          <Hooks />
          <Reducer />
          <Effect />
          <Ref />
        </header>
      </div>
    );
  }
}

export default App;
